@extends('layouts.publicLayout.public-template')
@section('main-content')

<section id="shop">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
            <h2>Search page</h2>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3">
                <div class="cat-list-wrap">

                
                    <div class="cat-list">
                        <h3  class="cat-list-name">Categories</h3>
                        <ul>
                        @foreach($categories as $category)
                            <li><a class="{{ request()->category == $category->slug ? 'active' : '' }}" href="{{ route ('shop.index', ['category' => $category->slug]) }}">{{ $category->name }}</a></li>
                        @endforeach
                        </ul>
                    </div>
                

                </div>
            </div>
            
            <div class="col-lg-9">
                <div class="filter-top">
                    <div class="row">
                        <div class="col-lg-8">
                            <p class="count-result">You searched for </p>
                        </div>
                        <div class="col-lg-4" align="right">
                            <p class="count-result">{{ $products->count() }} results found</p>
                        </div>
                    </div>
                </div>
                <!-- <div id="shop-top-filter" class="filter-btm">
                    <div class="row">
                    <div class="col-lg-8">
                        <form class="">
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>Colour</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>Price</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" id="exampleFormControlSelect1">
                            <option>Filter</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select>
                        </div>
                        </form>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-white reset-btn">Reset Filter<img src="{{ url('public/img/icons/close-icon.svg') }}"></button>
                    </div>
                    </div>
                </div> -->
                <div class="product-wrap">
                    @forelse ($products as $product)
                        <div class="product">
                            <a href="{{ url('product-details/'.$product->id) }}">
                            <div class="product-img">
                                <img src="{{ $product->product_images[0]['product_images'] }}">
                                <div class="overlay">
                                    <div class="p-actn">
                                        <button class="btn btn-light">View Product</button>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <div class="product-info">
                                <p class="product-name"><a href="{{ url('product-details/'.$product->id) }}">{{ Str::limit($product->name, 25) }}</a></p>
                                <p class="product-price">&#8358;{{ number_format($product->amount) }}</p>
                            </div>
                        </div>
                    @empty
                    <div class="shop-empty-state">No products found!</div>
                    @endforelse
                </div>
                <div class="row">
                    <div class="col-12" align="center">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-3">

            </div>
        </div>
    </div>
</section>

@endsection