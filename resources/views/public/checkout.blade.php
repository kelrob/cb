@extends('layouts.publicLayout.public-template')
@section('main-content')


    <section id="product-process">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7">
                    <div class="checkout-info-wrap">
                        <h2>Checkout</h2>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <p>{{ $errors->first() }}</p>
                            </div>
                        @endif
                        @auth
                        @else
                            <p>If you already have an account, Login below to chekout</p>
                            <div class="checkout-options">
                                <a href="{{ route('login') }}"><button class="btn btn-dark chk-login-btn">Login</button></a>
                                <span>OR</span>
                            </div>
                            <p>Fill the form below to checkout as a guest</p>
                        @endauth
                        <form method="GET">
                            <div class="row">

                                @isset(request()->d)
                                    <input type="hidden" name="d" value={{ request()->d }} />
                                @endisset

                                <div class="form-group col-6">
                                    <label for="exampleInputEmail1">First name<span>*</span></label>
                                    <input type="text" onkeyup="setFirstName()" name="firstname" class="form-control"
                                    id="firstname" @auth value={{ strtok(Auth::user()->name, ' ') }} disabled @else
                                        @isset(request()->firstname) value={{ request()->firstname }} @endisset
                                    @endauth aria-describedby="emailHelp">
                            </div>
                            <div class="form-group col-6">
                                <label for="exampleInputPassword1">Last name<span>*</span></label>
                                <input type="text" onkeyup="setLastName()" name="lastname" @auth
                                    value={{ strstr(Auth::user()->name, ' ') }} disabled @else
                                        @isset(request()->lastname) value={{ request()->lastname }} @endisset
                                    @endauth class="form-control" id="lastname">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="exampleInputEmail1">Email address<span>*</span></label>
                                <input type="text" name="email" onkeyup="setEmail()" class="form-control" id="email"
                                @auth value={{ Auth::user()->email }} disabled @else
                                    @isset(request()->email) value={{ request()->email }} @endisset
                                @endauth aria-describedby="emailHelp">
                        </div>
                        <div class="form-group col-6">
                            <label for="exampleInputPassword1">Phone number<span>*</span></label>
                            <input type="text" class="form-control" name="phone" @auth
                                value="{{ Auth::user()->phone }}" @else @isset(request()->phone)
                                    value={{ request()->phone }} @endisset @endauth onkeyup="setPhone()" id="phone">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-12">
                                <label for="exampleInputEmail1">Select Shipping Zone <span>*</span></label>
                                <select class="form-control" name="shipping_zone"
                                    onchange="setShippingZone(), this.form.submit()" id="shipping_zone_val">
                                    @isset(request()->shipping_zone)
                                        <option value="{{ explode(',', request()->shipping_zone)[0] }}">
                                            {{ explode(',', request()->shipping_zone)[2] }}</option>
                                    @else
                                        <option value="">Please Select</option>
                                    @endisset
                                    @foreach ($zones as $zone)
                                        <option
                                            value="{{ $zone->id . ',' . $zone->amount . ',' . $zone->zone_name }}">
                                            {{ $zone->zone_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group col-12">
                                <label for="exampleFormControlTextarea1">Delivery address<span>*</span></label>
                                <textarea class="form-control" name="delivery_address" id="delivery_address" rows="3"
                                    onkeyup="setDeliveryAddress()">@auth{{ Auth::user()->home_address }}@else @isset(request()->delivery_address){{ request()->delivery_address }}@endisset @endauth</textarea>
                            </div>
                        </div>
                        @auth
                            {{-- <div class="row">
                                    <div class="form-group form-check col-12">
                                        <label for="exampleFormControlTextarea1">Billing Address</label>
                                        <div class="form-check fm-radio">
                                            <input class="form-check-input chk-radio" type="radio" name="exampleRadios"
                                                id="exampleRadios1" value="option1" checked>
                                            <label class="form-check-label radio-label" for="exampleRadios1">
                                                Same as shipping address
                                            </label>
                                        </div>
                                        <div class="form-check fm-radio">
                                            <input class="form-check-input chk-radio" type="radio" name="exampleRadios"
                                                id="exampleRadios2" value="option2">
                                            <label class="form-check-label radio-label" for="exampleRadios2">
                                                Use a different address
                                            </label>
                                        </div>
                                    </div>
                                </div> --}}
                        @endauth


                        <div class="option2 box">
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="exampleInputEmail1">First name</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" onke
                                        aria-describedby="emailHelp">
                                </div>
                                <div class="form-group col-6">
                                    <label for="exampleInputPassword1">Last name</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp">
                                </div>
                                <div class="form-group col-6">
                                    <label for="exampleInputPassword1">Phone number</label>
                                    <input type="text" class="form-control" id="exampleInputPassword1">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="exampleInputEmail1">Select Shipping Zone <span>*</span></label>
                                    <select class="form-control" id="shipping_zone">
                                        <option value="">Please Select</option>
                                        @foreach ($zones as $zone)
                                            <option value="{{ $zone->id }}">{{ $zone->zone_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="exampleFormControlTextarea1">Delivery address</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1"
                                        rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="cart-summ">
                    <h2>Your Order <span><img src="{{ url('/public/img/icons/back-icon.svg') }}"><a
                                href="{{ route('cart.index') }}">Back to cart</a></span></h2>
                    <div class="cart-prdt-sum">

                        @foreach (Cart::content() as $item)
                            <div class="cart-s-prdt">
                                <div class="media">
                                    <div class="cart-s-prdt-img mr-3">
                                        <img src="{{ $item->model->product_images[0]['product_images'] }}">
                                    </div>
                                    <div class="media-body">
                                        <h5 class="mt-0">{{ $item->model->name }}
                                            <span>&#8358;{{ number_format($item->subtotal) }}</span>
                                        </h5>
                                        <p class="cart-prdt-qty">Quantity: {{ $item->qty }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <div class="cart-summ-info">
                        <div class="cart-item-wrap">
                            <p class="cart-item">Subtotal <span>&#8358;{{ Cart::subtotal() }}</span></p>
                            <p class="cart-item">Taxes (7.5%) <span>&#8358;{{ Cart::tax() }}</span></p>
                            <p class="cart-item">Shipping
                                <span id="shipping_cost">₦
                                    @isset(request()->shipping_zone)
                                        {{ number_format(explode(',', request()->shipping_zone)[1]) }}
                                    @else
                                        {{ 0 }}
                                    @endisset
                                </span>
                            </p>
                            @if (request()->d)
                                <p>Discount <span>{{ get_discount(request()->d) }}</span></p>
                            @endif

                        </div>
                        <p class="cart-total">Total
                            <span>&#8358;{{ number_format(intval(preg_replace('/[^\d. ]/', '', Cart::total())) -(request()->d ? get_amount(request()->d, Cart::total()) : 0) +(request()->shipping_zone == '' ? 0 : explode(',', request()->shipping_zone)[1])) }}</span>
                        </p>

                        @auth
                            <form method="POST" action="{{ url('pay/user') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="amount"
                                    value={{ intval(preg_replace('/[^\d. ]/', '', Cart::total())) -(request()->d ? get_amount(request()->d, Cart::total()) : 0) +(request()->shipping_zone == null ? 0 : explode(',', request()->shipping_zone)[1]) }} />
                                <input type="hidden" name="payment_method" value="both" />
                                <input type="hidden" name="description" value="Payment of Product" />
                                <input type="hidden" name="product_name" value="Product Name" />
                                <input type="hidden" name="country" value="NG" />
                                <input type="hidden" name="metadata[]" value="{{ Cart::content() }}" />
                                <input type="hidden" name="currency" value="NGN" />
                                <input type="hidden" name="email" value="{{ Auth::user()->email }}" />
                                <input type="hidden" name="firstname" value="{{ split_name(Auth::user()->name)[0] }}" />
                                <input type="hidden" name="lastname" value="{{ split_name(Auth::user()->name)[2] }}" />
                                <input type="hidden" name="phonenumber" value="{{ Auth::user()->phone }}" />
                                <input type="hidden" name="deliveryaddress" value="{{ Auth::user()->home_address }}" />
                                <input type="hidden" id="payment_shipping_zone" name="paymentShippingZone" />

                                <button type="submit" id="pay-btn"
                                    style=" background-color: #a20b37; color: #fff; border-style: none;"
                                    class="btn btn-danger pay-btn">Pay
                                    now
                                </button>
                            </form>

                        @else
                            <form method="POST" action="{{ url('pay/guest') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="amount"
                                    value={{ str_replace(',', '', Cart::total()) -(request()->d ? get_amount(request()->d, Cart::total()) : 0) +(request()->shipping_zone == null ? 0 : explode(',', request()->shipping_zone)[1]) }} />
                                <input type="hidden" name="payment_method" value="both" />
                                <input type="hidden" name="description" value="Payment of Product" />
                                <input type="hidden" name="product_name" value="Product Name" />
                                <input type="hidden" name="country" value="NG" />
                                <input type="hidden" name="metadata[]" value="{{ Cart::content() }}" />
                                <input type="hidden" name="currency" value="NGN" />
                                <input type="hidden" id="payment_email" name="email" />
                                <input type="hidden" id="payment_firstname" name="firstname" />
                                <input type="hidden" id="payment_lastname" name="lastname" />
                                <input type="hidden" id="payment_phone" name="phonenumber" />
                                <input type="hidden" id="payment_delivery_address" name="deliveryaddress" />
                                <input type="hidden" id="payment_shipping_zone" name="paymentShippingZone" />


                                <button type="submit" id="pay-btn" disabled
                                    style=" background-color: #a20b37; color: #fff; border-style: none;"
                                    class="btn btn-danger pay-btn">Pay
                                    now
                                </button>
                            </form>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    const activatePaymentButton = () => {

        const firstName = $('#firstname').val();
        const lastName = $('#lastname').val();
        const email = $('#email').val();
        const phone = $('#phone').val();
        const deliveryAddress = $('#delivery_address').val();
        const shippingZone = $('#shipping_zone_val').val();

        if (firstName && lastName && email && phone && deliveryAddress && shippingZone) {
            $('#pay-btn').removeAttr('disabled');
        } else {
            $('#pay-btn').attr('disabled', 'disabled');
        }

        console.log(firstName, lastName, email, phone, deliveryAddress, shippingZone);

        $('#payment_firstname').val(firstName);
        $('#payment_lastname').val(lastName);
        $('#payment_email').val(email);
        $('#payment_phone').val(phone);
        $('#payment_delivery_address').val(deliveryAddress);


    }

    const setFirstName = () => {
        $('#payment_firstname').val($('#firstname').val());
        activatePaymentButton();
    }

    const setLastName = () => {
        $('#payment_lastname').val($('#lastname').val());
        activatePaymentButton();
    }

    const setEmail = () => {
        $('#payment_email').val($('#email').val());
        activatePaymentButton();
    }

    const setPhone = () => {
        $('#payment_phone').val($('#phone').val());
        activatePaymentButton();
    }

    const setDeliveryAddress = () => {
        $('#payment_delivery_address').val($('#delivery_address').val());
        activatePaymentButton();
    }

    const setShippingZone = () => {

        let arr = $('#shipping_zone_val').val().split(',');

        $('#payment_shipping_zone').val(arr[0]);
        //$('#shipping_cost').text(`₦${arr[1]}`);
        activatePaymentButton();
    }


    setShippingZone();
</script>

@endsection
