@extends('layouts.publicLayout.public-template')
@section('main-content')

    <section id="shop">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $categoryName }}</h2>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-3">
                    <div class="cat-list-wrap">


                        <div class="cat-list">
                            <h3 class="cat-list-name">Categories</h3>
                            <ul>
                                @foreach ($categories as $category)
                                    <li><a class="{{ request()->category == $category->slug ? 'active' : '' }}"
                                            href="{{ route('shop.index', ['category' => $category->slug]) }}">{{ $category->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>


                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="filter-top">
                        <div class="row">
                            <div class="col-lg-8">
                                <p class="count-result">{{ number_format($productCount) }} items found</p>
                            </div>
                            <div class="col-lg-4">
                                <div class="filter">
                                    <!-- <span onclick="myFunction()"><img class="filter-icon" src="{{ url('public/img/icons/filter-icon.svg') }}"><p>Toggle Filter</p></span> -->
                                    <div class="dropdown">
                                        <span id="dropdownMenu2" data-toggle="dropdown">
                                            <p>Sort by Price</p><img class="icon-chevron"
                                                src="{{ url('public/img/icons/chevron-down-black.svg') }}">
                                        </span>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <a class="dropdown-item"
                                                href="{{ url('shop/' . $type . '?sort=low_high') }}">Low
                                                to High</a>
                                            <a class="dropdown-item"
                                                href="{{ url('shop/' . $type . '?sort=high_low') }}">High
                                                to Low</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div id="shop-top-filter" class="filter-btm">
                                            <div class="row">
                                            <div class="col-lg-8">
                                                <form class="">
                                                <div class="form-group">
                                                    <select class="form-control" id="exampleFormControlSelect1">
                                                    <option>Colour</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control" id="exampleFormControlSelect1">
                                                    <option>Price</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control" id="exampleFormControlSelect1">
                                                    <option>Filter</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    </select>
                                                </div>
                                                </form>
                                            </div>
                                            <div class="col-lg-4">
                                                <button class="btn btn-white reset-btn">Reset Filter<img src="{{ url('public/img/icons/close-icon.svg') }}"></button>
                                            </div>
                                            </div>
                                        </div> -->
                    <div class="product-wrap">
                        @forelse ($products as $product)
                            <div class="product">
                                @if ($product->sale_amount != null)
                                    <div class="sale-label">Sale</div>
                                @endif
                                <a href="{{ url('product-details/' . $product->id) }}">
                                    <div class="product-img">
                                        <img src="{{ $product->product_images[0]['product_images'] }}">
                                        <div class="overlay">
                                            <div class="p-actn">
                                                <button class="btn btn-light">View Product</button>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="product-info">
                                    <p class="product-name"><a
                                            href="{{ url('product-details/' . $product->id) }}">{{ Str::limit($product->name, 25) }}</a>
                                    </p>
                                    @if ($product->sale_amount != null)
                                        <p class="product-price">
                                            <span>&#8358; {{ number_format($product->amount) }}</span>
                                            &#8358; {{ number_format($product->sale_amount) }}
                                        </p>
                                    @else
                                        <p class="product-price">&#8358;{{ number_format($product->amount) }}</p>
                                    @endif
                                </div>
                            </div>
                        @empty
                            <div class="shop-empty-state">No products found!</div>
                        @endforelse
                    </div>
                    <div class="row">
                        <div class="col-12" align="center">
                            {{ $products->appends(request()->input())->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
