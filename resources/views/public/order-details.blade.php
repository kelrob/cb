@extends('layouts.publicLayout.public-template')
@section('main-content')

    <section id="user-acct">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="user-acct-menu">

                                <div class="user-acct-info">
                                    <div class="media">
                                        <!-- <div class="user-acct-img">

                                                                                                                                                                                                                                          </div> -->
                                        <div class="media-body">
                                            <h5>Hi,</h5>
                                            <p>{{ Auth::user()->name }}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="user-menu">
                                    <ul class="uam">
                                        <li><a href="{{ url('/account') }}">My Details</a></li>
                                        <li><a href="{{ url('/orders') }}">My Orders</a></li>
                                        <li><a href="#">Need help?</a></li>
                                        <li><a href="#">How do I make a return?</a></li>
                                        <li><a href="{{ route('logout') }}"
                                                onclick="event.preventDefault(); document.getElementById('user-logout-form').submit();">Sign
                                                out
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-head">
                                    <div class="row">
                                        <div class="col">
                                            <h2>Order details</h2>
                                        </div>
                                        <div class="col">
                                            <p class="back-text"><a href="{{ url('/orders') }}"> Back to orders
                                                    page</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body card-body-full">
                                    <div class="order-info">

                                    </div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Item</th>
                                                <th scope="col">Unit Price</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($orders as $order)
                                                @foreach (json_decode($order->payment_history->meta) as $key => $value)
                                                    <tr>
                                                        <td scope="row">

                                                            <img
                                                                src="{{ get_product($value->id)->product_images[0]->product_images }}" />
                                                            <div class="tbl-prdt-d">
                                                                <p class="tbl-prdt-price">
                                                                    {{ get_product($value->id)->name }}
                                                                </p>
                                                                <p class="tbl-order-id">
                                                                    {{ get_color($value->options->color)->name }}
                                                                </p>
                                                            </div>
                                                        </td>
                                                        <td>₦{{ number_format($value->price) }}</td>
                                                        <td>{{ number_format($value->qty) }}</td>
                                                        <td>₦{{ number_format($value->subtotal) }}</td>
                                                    </tr>
                                                @endforeach
                                                {{-- @foreach (json_decode($order->payment_history->meta) as $data)
                                                    {{ count($data) }}
                                                    <tr>
                                                        <td scope="row">
                                                            <img src="img/wc2.jpg">
                                                            <div class="tbl-prdt-d">
                                                                <p class="tbl-prdt-price">Name of product</p>
                                                                <p class="tbl-order-id">Wanzy Electrical Services Ltd</p>
                                                            </div>
                                                        </td>
                                                        <td>₦780,000.00</td>
                                                        <td>1</td>
                                                        <td>₦780,000.00</td>
                                                    </tr>
                                            @endforeach --}}

                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
