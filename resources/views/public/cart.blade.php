@extends('layouts.publicLayout.public-template')
@section('main-content')

    <section id="product-process">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7">
                    <div class="shopping-cart">

                        @if (session('success'))
                            <div class="alert alert-success mb-5">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger mb-5">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (Cart::count() > 0)

                            <h2>Shopping Cart <span>{{ Cart::count() }} item(s) in Shopping Cart</span></h2>
                            <table class="table table-borderless">

                                <tbody>
                                    @foreach (Cart::content() as $item)

                                        <tr>
                                            <td>
                                                <div class="cart-p-info">
                                                    <div class="cart-p-img">
                                                        <img
                                                            src="{{ $item->model->product_images[0]['product_images'] }}">
                                                    </div>
                                                    <p class="cart-p-name"><a
                                                            href="{{ url('product-details/' . $item->model->id) }}">{{ $item->model->name }}</a>
                                                    </p>

                                                    <p class="p-info">
                                                        {{ get_color($item->options->color)->name }}
                                                    </p>

                                                </div>
                                            </td>
                                            <td align="center">
                                                <p class="cart-p-price">&#8358;{{ number_format($item->subtotal) }}</p>
                                            </td>

                                            <td align="center">
                                                <div class="form-group cart-qty">
                                                    <select class="quantity form-control" data-id="{{ $item->rowId }}"
                                                        data-productQuantity="{{ $item->model->quantity }}">
                                                        @for ($i = 1; $i < 5 + 1; $i++)
                                                            <option {{ $item->qty == $i ? 'selected' : '' }}>
                                                                {{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </td>

                                            <td>
                                                <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
                                                    @csrf
                                                    {{ method_field('DELETE') }}

                                                    <button type="submit" class="btn cart-del-btn"><img
                                                            class="c-close-icon"
                                                            src="{{ url('/public/img/icons/close-icon.svg') }}"></button>
                                                </form>
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>

                        @else

                            <h3 class="cart-empty-state">No items in Cart!</h3>
                            <a href="{{ route('shop.index') }}"><button type="button"
                                    class="btn cart-continue-shp">Continue Shopping</button></a>

                        @endif

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="cart-summ">
                        <h2>Summary <span><img src="{{ url('/public/img/icons/back-icon.svg') }}"><a
                                    href="{{ route('shop.index') }}">Back to shop</a></span></h2>
                        <div class="cart-summ-info">
                            <p>Subtotal <span>&#8358;{{ Cart::subtotal() }}</span></p>
                            <p>Taxes (7.5%) <span>&#8358;{{ Cart::tax() }}</span></p>
                            @if (request()->d)
                                <p>Discount <span>{{ get_discount(request()->d) }}</span></p>
                            @endif
                            <p class="cart-total-n">Total
                                {{-- {{ dd(intval(preg_replace('/[^\d. ]/', '', Cart::total())) - get_amount(request()->d)) }} --}}
                                <span>&#8358;{{ number_format(intval(preg_replace('/[^\d. ]/', '', Cart::total())) - (request()->d ? get_amount(request()->d, Cart::total()) : 0)) }}</span>
                            </p>
                            <p class="cart-ship-info">Shipping is calculated on checkout page</p>
                            <label for="coupon">Have a Coupon Code?</label>
                            <form class="form-inline">
                                <div class="form-group mb-2">
                                    <input type="text" class="form-control" name="d" id="coupon">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">Apply</button>
                            </form>
                            <form class="cart-pr-fm">
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input cart-checkbox" checked disabled
                                        id="exampleCheck1">
                                    <label class="form-check-label cart-cbox-label" for="exampleCheck1">I agree to <a
                                            href="#">Terms & Conditions</a></label>
                                </div>
                                <a href="{{ url(request()->d ? 'checkout?d=' . request()->d : 'checkout') }}"><button
                                        type="button" class="btn btn-danger">Checkout</button></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        (function() {
            const classname = document.querySelectorAll('.quantity')
            Array.from(classname).forEach(function(element) {
                element.addEventListener('change', function() {
                    const id = element.getAttribute('data-id')
                    const productQuantity = element.getAttribute('data-productQuantity')
                    axios.patch(`/cart/${id}`, {
                            quantity: this.value,
                            productQuantity: productQuantity
                        })
                        .then(function(response) {
                            // console.log(response);
                            window.location.href = '{{ route('cart.index') }}'
                        })
                        .catch(function(error) {
                            // console.log(error);
                            window.location.href = '{{ route('cart.index') }}'
                        });
                })
            })
        })();
    </script>

@endsection
