@extends('layouts.publicLayout.public-template')
@section('main-content')

    <section id="user-acct">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="user-acct-menu">

                                <div class="user-acct-info">
                                    <div class="media">
                                        <!-- <div class="user-acct-img">

                                                                                                                                                                      </div> -->
                                        <div class="media-body">
                                            <h5>Hi,</h5>
                                            <p>{{ Auth::user()->name }}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="user-menu">
                                    <ul class="uam">
                                        <li><a href="{{ url('/account') }}">My Details</a></li>
                                        <li><a href="{{ url('/orders') }}">My Orders</a></li>
                                        <li><a href="#">Need help?</a></li>
                                        <li><a href="#">How do I make a return?</a></li>
                                        <li><a href="{{ route('logout') }}"
                                                onclick="event.preventDefault(); document.getElementById('user-logout-form').submit();">Sign
                                                out
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-head">
                                    <h2>Personal Information</h2>
                                </div>
                                <div class="card-body">
                                    @if (session('success'))
                                        <div class="alert alert-success mb-5">
                                            <a href="#" class="close" data-dismiss="alert"
                                                aria-label="close">&times;</a>
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <form method="POST" action="{{ url('update-user') }}">
                                        @csrf
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>First name</label>
                                                    <input type="text" class="form-control" disabled
                                                        placeholder="First name"
                                                        value={{ strtok(Auth::user()->name, ' ') }}>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>Last name</label>
                                                    <input type="text" disabled class="form-control"
                                                        placeholder="Last name"
                                                        value={{ strstr(Auth::user()->name, ' ') }}>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>Email address</label>
                                                    <input type="email" name="email" class="form-control"
                                                        placeholder="Email" value="{{ Auth::user()->email }}">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label>Phone number</label>
                                                    <input type="text" class="form-control" placeholder="Phone number"
                                                        name="phone" value="{{ Auth::user()->phone }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Address</label>
                                                    <textarea class="form-control" id="exampleFormControlTextarea1"
                                                        rows="3"
                                                        name="home_address">{{ Auth::user()->home_address }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Gender (optional)</label>
                                                    <select class="form-control" name="gender">
                                                        @if (Auth::user()->gender != null)
                                                            <option value="{{ Auth::user()->gender }}">
                                                                {{ ucfirst(Auth::user()->gender) }}</option>
                                                        @endif
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-danger auth-btn">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
