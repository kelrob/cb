@extends('layouts.publicLayout.public-template')
@section('main-content')

    <section id="single-product">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-7 sp-lhs">
                    <div class="thumb-wrap">

                        <div id="thmnl" class="thumbnail">
                            @foreach ($product->product_images as $image)
                                <div class="p-thumb product-image-thumbnail"><img id="1" class="thumb"
                                        src="{{ $image->product_images }}" onclick='preview(this)'></div>
                            @endforeach
                            <!-- <div class="p-thumb product-image-thumbnail"><img id="1" class="thumb" src="img/1.png" onclick='preview(this)'></div>
                    <div class="p-thumb product-image-thumbnail"><img id="2" class="thumb"  src="img/3.png" onclick='preview(this)'></div>
                    <div class="p-thumb product-image-thumbnail"><img id="3" class="thumb"  src="img/4.png" onclick='preview(this)'></div> -->
                        </div>

                        <div id="product-image" class="main-img">
                            <img id='0' class='image' src="{{ $product->product_images[0]['product_images'] }}">
                        </div>
                    </div>

                </div>
                <div class="col-lg-5 sp-rhs">

                    <div class="sproduct-info">
                        @if (session('success'))
                            <div class="alert alert-success mb-5">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger mb-5">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="sp-wrap">
                            <p class="sp-cat">{{ $product->category->name }}</p>
                            <h1 class="sp-name">{{ $product->name }}</h1>
                            <div class="sp-rating">
                                <div class="starrs">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </div>
                                <p class="sp-rating-score">4.5 / 5 <span><a href="#">See all reviews</a></span></p>
                            </div>
                            <p class="sp-price">&#8358;{{ number_format($product->amount) }}</p>
                            <div class="sp-color-wrap">
                                <p class="sp-label">AVAILABLE SHADE / COLOUR</p>
                                <div class="sp-color">
                                    @foreach ($colors as $color)
                                        <div style="background:{{ $color->color_code }}!important" class="sc1">
                                        </div>
                                    @endforeach
                                </div>
                                <form action="{{ route('cart.store') }}" method="POST">
                                    <div class="form-group">
                                        <select class="form-control" required name="color" id="color">
                                            <option value="">Select Colour</option>
                                            @foreach ($colors as $color)
                                                <option value="{{ $color->id }}">{{ $color->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <input type="hidden" name="name" value="{{ $product->name }}">
                                    <input type="hidden" name="price"
                                        value="{{ intval(preg_replace('/[^\d.]/', '', $product->amount)) }}">

                                    <button class="btn btn-danger sp-add-to-cart-btn">Add to cart</button>
                                </form>
                            </div>
                            <!-- <div class="sp-quantity">
                      <p class="sp-label">Select quantity</p>
                      <div class="input-group">
                        <div class="button minus">
                          <button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">
                            <i class="ti-minus"></i>
                          </button>
                        </div>
                        <input type="text" name="quant[1]" class="input-number" data-min="1" data-max="1000" value="5">
                        <div class="button plus">
                          <button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[1]">
                            <i class="ti-plus"></i>
                          </button>
                        </div>
                      </div>
                    </div> -->
                            {{-- <form action="{{ route('cart.store') }}" method="POST">

              <button class="btn btn-danger sp-add-to-cart-btn">Add to cart</button>
            </form> --}}
                            <div class="sp-share-op">
                                <p class="sp-label">Share Product</p>
                                <a href="#"><img src="{{ url('public/img/icons/facebook.svg') }}"></a>
                                <a href="#"><img src="{{ url('public/img/icons/twitter.svg') }}"></a>
                                <a href="#"><img src="{{ url('public/img/icons/instagram.svg') }}"></a>
                                <a href="#"><img src="{{ url('public/img/icons/link-icon.svg') }}"></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <section id="product-info">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Product Information</h1>
                    <p>{{ strip_tags($product->description) }}</p>

                    <div class="line"></div>

                    <div class="row">

                        <div class="col-lg-4">
                            <h1>Customer Ratings and Reviews</h1>

                            <div class="c-rating">
                                <h2>Avr. Rating <span>All Time</span></h2>
                                <div class="avr-rating-star">
                                    <h3 class="rating-score">4.0</h3>
                                    <div class="starrs">
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star"></span>
                                        <span class="fa fa-star"></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <p class="rating-count">(4298 Reviews)</p>
                            </div>
                        </div>

                        <div class="col-lg-4 mt-2">

                            <div>
                                <div class="progress-label">5</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div>
                                <div class="progress-label">4</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div>
                                <div class="progress-label">3</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div>
                                <div class="progress-label">2</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <div>
                                <div class="progress-label">1</div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-4 mt-2" align="center">
                            <button class="btn btn-danger w-review-btn">Rate this product</button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>


    <section id="rv-products">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <h1>You might also like these Products</h1>
                    <div class="product-wrap">

                        @foreach ($products as $product)
                            <div class="product">
                                <a href="{{ url('product-details/' . $product->id) }}">
                                    <div class="product-img">
                                        <img src="{{ $product->product_images[0]['product_images'] }}">
                                        <div class="overlay">
                                            <div class="p-actn">
                                                <button class="btn btn-light">View Product</button>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="product-info">
                                    <p class="product-cat">{{ $product->category->name }}</p>
                                    <p class="product-name"><a
                                            href="product-details.html">{{ Str::limit($product->name, 25) }}</a></p>
                                    <p class="product-price">&#8358;{{ number_format($product->amount) }}</p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
