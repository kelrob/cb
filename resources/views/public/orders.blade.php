@extends('layouts.publicLayout.public-template')
@section('main-content')

    <section id="user-acct">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="user-acct-menu">

                                <div class="user-acct-info">
                                    <div class="media">
                                        <!-- <div class="user-acct-img">

                                                                                                                                                          </div> -->
                                        <div class="media-body">
                                            <h5>Hi,</h5>
                                            <p>{{ Auth::user()->name }}</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="user-menu">
                                    <ul class="uam">
                                        <li><a href="{{ url('/account') }}">My Details</a></li>
                                        <li><a href="{{ url('/orders') }}">My Orders</a></li>
                                        <li><a href="#">Need help?</a></li>
                                        <li><a href="#">How do I make a return?</a></li>
                                        <li><a href="{{ route('logout') }}"
                                                onclick="event.preventDefault(); document.getElementById('user-logout-form').submit();">Sign
                                                out
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-head">
                                    <h2>All Orders</h2>
                                </div>
                                <div class="card-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Product</th>
                                                <th scope="col">Date</th>
                                                <th scope="col">Order Status</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($orders as $order)
                                                <tr>
                                                    <td scope="row">
                                                        <!-- <img src="img/"> -->
                                                        <div class="tbl-prdt-d">
                                                            <p class="tbl-order-id">Order ID:
                                                                {{ $order->order_id }}
                                                            </p>
                                                            <p class="tbl-prdt-price">
                                                                ₦{{ number_format($order->payment_history->amount) }}
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>{{ $order->created_at->format('d M Y') }}</td>
                                                    <td>{{ ucfirst($order->status) }}</td>
                                                    <td class="table-action"><a
                                                            href="{{ url('/order-details/' . $order->id) }}">View</a>
                                                    </td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
