<?php

use App\Models\Color;
use App\Models\Coupon;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

if (!function_exists('user')) {
    function user($column)
    {
        return Auth::user()->$column;
    }
}

if (!function_exists('string_to_array')) {
    function string_to_array($string)
    {
        return json_decode($string);
    }
}

if (!function_exists('get_color')) {
    function get_color($id)
    {
        return Color::where('id', $id)->first();
    }
}

if (!function_exists('split_name')) {
    function split_name($name)
    {
        $parts = explode(' ', $name); // $meta->post_title
        $name_first = array_shift($parts);
        $name_last = array_pop($parts);
        $name_middle = trim(implode(' ', $parts));

        return array($name_first, $name_middle, $name_last);
    }
}

if (!function_exists('get_amount')) {
    function get_amount($code, $total)
    {
        $total = intval(preg_replace('/[^\d. ]/', '',$total));
        $coupon = Coupon::where('code', $code)->where('is_active', true)->first();
        if ($coupon) {
            return ($coupon->percentage_off / 100) * $total;
        }
        return 0;

    }
}

if (!function_exists('get_discount')) {
    function get_discount($code)
    {

        $coupon = Coupon::where('code', $code)->where('is_active', true)->first();
        if ($coupon) {
            return $coupon->percentage_off . '%';
        }
        return 0 . '%';
    }
}

if (!function_exists('get_product')) {
    function get_product($id)
    {
        $product = Product::with('product_images')->where('id', $id)->first();
        return $product;
    }
}

if (!function_exists('get_color')) {
    function get_color($id)
    {
        $color = Color::where('id', $id)->first();
        return $color;
    }
}
