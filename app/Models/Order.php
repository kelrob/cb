<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order_status()
    {
        return $this->hasOne(User::class);
    }

    public function payment_history()
    {
        return $this->belongsTo(PaymentHistory::class);
    }
}
