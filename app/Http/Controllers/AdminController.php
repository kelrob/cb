<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use App\Models\Banner;
use App\Models\SaleBanner;
use App\Models\Category;
use App\Models\Color;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Newsletter;
use App\Models\Shipping;
use App\Models\SubCategory;
use App\Models\Testimonial;
use App\Http\Requests\AddZoneRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $productCount = Product::all()->count();
        $categoryCount = Category::all()->count();
        $orders = Order::all();
        $users = User::all();
        return view('admin.index', compact('productCount', 'categoryCount', 'orders', 'users'));
    }

    public function products()
    {
        $products = Product::with('user')->orderBy('created_at', 'DESC')->get();
        return view('admin.products', compact('products'));
    }

    public function newProduct()
    {
        $colors = Color::get();
        $categories = Category::orderBy('created_at', 'DESC')->get();
        $subCategories = SubCategory::orderBy('created_at', 'DESC')->get();
        return view('admin.new-product', compact('categories', 'subCategories', 'colors'));
    }

    public function viewProduct($id)
    {
        $product = Product::with('user', 'product_images')->where('id', $id)->first();

        return view('admin.view-product', compact('product'));
    }

    public function editProduct($id)
    {
        $colors = Color::get();
        $categories = Category::orderBy('created_at', 'DESC')->get();
        $product = Product::with('user', 'product_images', 'category')->where('id', $id)->first();


        return view('admin.edit-product', compact('product', 'categories', 'colors'));
    }

    public function orders()
    {
        $orders = Order::orderBy('created_at', 'DESC')->get();
        return view('admin.orders', compact('orders'));
    }

    public function categories()
    {
        $categories = Category::with('products')->orderBy('created_at', 'DESC')->get();
        return view('admin.categories', compact('categories'));
    }

    public function users()
    {
        $users = User::orderBy('created_at', 'DESC')->get();
        return view('admin.users', compact('users'));
    }

    public function banners()
    {
        $banners = Banner::orderBy('created_at', 'DESC')->get();
        return view('admin.banners', compact('banners'));
    }

    public function newBanner()
    {
        return view('admin.new-banner');
    }

    public function editBanner($id)
    {
        $decodedId = base64_decode($id);
        $banner = Banner::where('id', $decodedId)->first();
        return view('admin.edit-banner', compact('banner'));
    }

    public function testimonials()
    {
        $testimonials = Testimonial::orderBy('created_at', 'DESC')->get();
        return view('admin.testimonials', compact('testimonials'));
    }

    public function newTestimonial()
    {
        return view('admin.new-testimonial');
    }

    public function editTestimonial($id)
    {
        $decodedId = base64_decode($id);
        $testimonial = Testimonial::where('id', $decodedId)->first();
        return view('admin.edit-testimonial', compact('testimonial'));
    }

    public function newCategory()
    {
        return view('admin.new-category');
    }

    public function subCategories()
    {
        $subCategories = SubCategory::with('products')->orderBy('created_at', 'DESC')->get();
        return view('admin.sub-categories', compact('subCategories'));
    }

    public function newSubCategory()
    {
        return view('admin.new-sub-category');
    }

    public function colors()
    {
        $colors = Color::orderBy('id', 'DESC')->get();
        return view('admin.colors', compact('colors'));
    }

    public function newColor()
    {
        return view('admin.new-color');
    }

    public function editColor($id)
    {
        $decodedId = base64_decode($id);
        $color = Color::where('id', $decodedId)->first();
        return view('admin.edit-color', compact('color'));
    }

    public function coupons()
    {
        $coupons = Coupon::orderBy('id', 'DESC')->get();
        return view('admin.coupons', compact('coupons'));
    }

    public function newCoupon()
    {
        return view('admin.new-coupon');
    }

    public function editCoupon($id)
    {
        $decodedId = base64_decode($id);
        $coupon = Coupon::where('id', $decodedId)->first();
        return view('admin.edit-coupon', compact('coupon'));
    }

    public function newsletter()
    {
        $newsletters = Newsletter::get();
        return view('admin.newsletter', compact('newsletters'));
    }

    // Shipping zones

    public function zones()
    {
        $zones = Shipping::get();
        return view('admin.zones', compact('zones'));
    }

    public function newZone()
    {
        return view('admin.new-zones');
    }

    public function editZone($id)
    {
        $decodedId = base64_decode($id);
        $zones = Shipping::where('id', $decodedId)->first();
        return view('admin.edit-zones', compact('zones'));
    }

    public function addZone(AddZoneRequest $request)
    {
        $newZone = Shipping::create([
            'zone_name' => $request->zone_name,
            'amount' => $request->amount,
        ]);

        if ($newZone) {
            return redirect()->back()->with('success', 'Shipping Zone Uploaded Successfully');
        }
    }

    public function updateZone(Request $request)
    {
        $zones = Shipping::where('id', $request->id)->first();

        $updateZone = $zones->update([
            'zone_name' => $request->zone_name,
            'amount' => $request->amount,
        ]);

        if ($updateZone) {
            return redirect()->back()->with('success', 'Shipping Zone Updated Successfully');
        }
    }

    public function deleteZone($id)
    {
        $zone = Shipping::where('id', $id)->first();
        $zone->delete();

        return true;
    }
}
