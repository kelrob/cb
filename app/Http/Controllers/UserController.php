<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function updateUser(Request $request) {
        $user = User::where('id', Auth::user()->id)->first();
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->home_address = $request->home_address;
        $user->gender = $request->gender;
        $user->save();

        return redirect()->back()->with('success', 'Profile Updated Successfully');
    }
}
