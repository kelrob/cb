<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\PaymentHistory;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use KingFlamez\Rave\Facades\Rave as Flutterwave;

class FlutterwaveController extends Controller
{
    public function initialize($type)
    {

        if (!request()->email || !request()->firstname || !request()->lastname || !request()->phonenumber) {
            return redirect()->back()->withErrors(['msg' => 'All fields are required']);
        }


        $reference = Flutterwave::generateReference();

        // Enter the details of the payment
        $data = [
            'payment_options' => 'card,banktransfer',
            'amount' => request()->amount,
            'email' => request()->email,
            'tx_ref' => $reference,
            'currency' => "NGN",
            'redirect_url' => route('callback'),
            'meta' => [
                'order_details' => request()->metadata[0],
                'type' => $type,
                'delivery_address' => request()->deliveryaddress,
                'phonenumber' => request()->phonenumber,
                'payment_shipping_zone' => request()->paymentShippingZone,
            ],
            'customer' => [
                'email' => request()->email,
                "name" => request()->firstname . ' ' . request()->lastname
            ],
            "customizations" => [
                "product_name" => request()->product_name,
            ],
        ];

        $payment = Flutterwave::initializePayment($data);

        if ($payment['status'] !== 'success') {
            // notify something went wrong
            return;
        }

        return redirect($payment['data']['link']);
    }

    public function callback()
    {



        $status = request()->status;

        //if payment is successful
        if ($status == 'successful') {

            $transactionID = Flutterwave::getTransactionIDFromCallback();
            $data = Flutterwave::verifyTransaction($transactionID);


            $payment = PaymentHistory::create([
                'customer_email' => $data['data']['customer']['email'],
                'meta' => $data['data']['meta']['order_details'],
                'status' => $data['data']['status'],
                'tx_ref' => $data['data']['tx_ref'],
                'flw_ref' => $data['data']['flw_ref'],
                'amount' => $data['data']['amount'],
                'app_fee' => $data['data']['app_fee'],
                'payment_date' => $data['data']['created_at'],
            ]);


            Order::create([
                'payment_history_id' => $payment->id,
                'order_id' => time(),
                'customer_email' => $data['data']['customer']['email'],
                'fullname' => $data['data']['customer']['name'],
                'delivery_address'=> $data['data']['meta']['delivery_address'],
                'phone'=> $data['data']['meta']['phonenumber'],
                'type' => $data['data']['meta']['type'],
                'shipping_zone_id' => $data['data']['meta']['payment_shipping_zone'],
            ]);

            Cart::destroy();

            return redirect()->to('/checkout-success');
        } elseif ($status == 'cancelled') {
            return redirect('/cart');
            //Put desired action/code after transaction has been cancelled here
        } else {
            //Put desired action/code after transaction has failed here
        }
        // Get the transaction from your DB using the transaction reference (txref)
        // Check if you have previously given value for the transaction. If you have, redirect to your successpage else, continue
        // Confirm that the currency on your db transaction is equal to the returned currency
        // Confirm that the db transaction amount is equal to the returned amount
        // Update the db transaction record (including parameters that didn't exist before the transaction is completed. for audit purpose)
        // Give value for the transaction
        // Update the transaction to note that you have given value for the transaction
        // You can also redirect to your success page from here

    }
}
