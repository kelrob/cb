<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Testimonial;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Color;
use App\Models\SaleBanner;
use App\Models\Newsletter;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function home()
    {
        $products = Product::limit(4)->inRandomOrder()->where('featured', true)->get();
        $testimonials = Testimonial::inRandomOrder()->get();
        $categories = Category::limit(8)->inRandomOrder()->get();
        $homeBanners = Banner::where('location', 'hero_banner')->get();
        $saleBanners = Banner::where('location', 'sale_banner')->get();
        $featuredBanners = Banner::where('location', '=', 'featured_banner')->get();
        return view('public/index')->with(compact('products', 'testimonials', 'categories', 'homeBanners', 'saleBanners', 'featuredBanners'));
    }

    public function productDetails($id)
    {
        $product = Product::whereId($id)->first();
        $productcolour = Product::find($id)->color;
        $colorArray = json_decode($productcolour, true);
        $colors = Color::whereIn('id', $colorArray)->get();
        $products = Product::limit(5)->inRandomOrder()->get();
        return view('public/product-details', compact('product', 'products', 'colors'));
    }

    public function checkoutSuccess()
    {
        return view('public/checkout-success');
    }

    public function index()
    {
        // dd($request->all());

        $pagination = 4;
        $categories = Category::all();

        if (request()->category) {
            $products = Product::with('category')->whereHas('category', function ($query) {
                $query->where('slug', request()->category);
            });
            $categoryName = optional($categories->where('slug', request()->category)->first())->name;
        } else {
            $products = Product::take(12);
            $categoryName = 'All Products';
        }

        if (request()->sort == 'low_high') {
            $products = $products->orderBy('amount')->paginate($pagination);
        } elseif (request()->sort == 'high_low') {
            $products = $products->orderBy('amount', 'desc')->paginate($pagination);
        } else {
            $products = $products->paginate($pagination);
        }

        $productCount = Product::all()->count();
        return view('public/shop')->with(compact('products', 'productCount', 'categories', 'categoryName'));
    }

    public function indexByType($type)
    {
        // dd($request->all());

        $pagination = 4;
        $categories = Category::all();

        if (request()->category) {

            if ($type == 'sale') {
                $products = Product::where('sale_amount', '!=', null)->with('category')->whereHas('category', function ($query) {
                    $query->where('slug', request()->category);
                });
            } else {
                $products = Product::where('type', $type)->with('category')->whereHas('category', function ($query) {
                    $query->where('slug', request()->category);
                });
            }


            $categoryName = optional($categories->where('slug', request()->category)->first())->name;
        } else {
            if ($type == 'sale') {
                $products = Product::where('sale_amount', '!=', null)->take(12);
            } else {
                $products = Product::where('type', $type)->take(12);
            }


            $categoryName = 'All Products';
        }

        if (request()->sort == 'low_high') {
            $products = $products->orderBy('amount')->paginate($pagination);
        } elseif (request()->sort == 'high_low') {
            $products = $products->orderBy('amount', 'desc')->paginate($pagination);
        } else {
            $products = $products->paginate($pagination);
        }

        if ($type == 'sale') {
            $productCount = Product::where('sale_amount', '!=', null)->get()->count();
        } else {
            $productCount = Product::where('type', $type)->get()->count();
        }

        return view('public/shop-type')->with(compact('products', 'productCount', 'categories', 'categoryName','type'));
    }

    public function search(Request $request)
    {
        $pagination = 12;
        $categories = Category::all();

        if (request()->category) {
            $products = Product::with('category')->whereHas('category', function ($query) {
                $query->where('slug', request()->category);
            });
            $categoryName = optional($categories->where('slug', request()->category)->first())->name;
        } else {
            $products = Product::where('name', 'like', '%' . $request->input('search') . '%')->paginate(12);
        }

        // if (request()->sort == 'low_high') {
        //     $products = $products->orderBy('amount')->paginate($pagination);
        // } elseif (request()->sort == 'high_low') {
        //     $products = $products->orderBy('amount', 'desc')->paginate($pagination);
        // } else {
        //     $products = $products->paginate($pagination);
        // }


        return view('public/search')->with(compact('products', 'categories'));
    }

    public function account()
    {
        return view('public/account');
    }

    public function orders()
    {
        $orders = Order::where('customer_email', Auth::user()->email)->orderBy('created_at', 'DESC')->get();

        return view('public/orders', compact('orders'));
    }

    public function orderDetails($orderId)
    {
        $orders = Order::with('payment_history')->where('customer_email', Auth::user()->email)->where('id', $orderId)->get();
        //dd(json_decode($orders->payment_history));
        return view('public/order-details', compact('orders'));
    }

    public function postemailSubscription(Request $request){

        if($request->isMethod('post')){
            $data = $request->all();
            // echo "<pre>"; print_r($data); die;
            $emailsub = new Newsletter;
            $emailsub->email = $data['sub'];
            $emailsub->save();

            return redirect('/')->with('flash_message_success','Email Subscription successful');
        }

        return view('/');
    }
}
